package restservice.controller;

import ScalaBusRoot.BusRoutesService;
import ScalaBusRoot.exception.FileWasDeletedException;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import spark.Request;
import restservice.JsonTransformer;
import restservice.exception.WrongParamTypeException;
import restservice.responce.ErrorResponse;
import restservice.responce.ServiceResponce;

import static spark.Spark.after;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.setPort;
/**
 * Created by andrew on 11/2/16.
 */
public class RoutesController {
    public static final String DEP_SID = "dep_sid";
    public static final String ARR_SID = "arr_sid";
    public static final String S_PARAM_IS_NOT_INTEGEER_TYPE = "%s param is not Integeer Type";
    public static final String APPLICATION_JSON = "application/json";

    public RoutesController() {
        setPort(8088);
        get("/api/direct", (req, res) -> {
            validateQuerryParams(req);
            String departure = req.queryParams(DEP_SID).toString();
            String arrival = req.queryParams(ARR_SID).toString();
            return new ServiceResponce(departure,
                    arrival,
                    BusRoutesService.isDirectRouteExists(departure, arrival));
        }, new JsonTransformer());

        after((req, res) -> {
            res.type(APPLICATION_JSON);
        });

        exception(WrongParamTypeException.class, (exception, request, response) -> {
            Gson gson = new Gson();
            response.status(400);
            response.type(APPLICATION_JSON);
            response.body(gson.toJson(new ErrorResponse(exception.getMessage(), WrongParamTypeException.class.getName())));
        });

        exception(FileWasDeletedException.class, (exception, request, response) -> {
            Gson gson = new Gson();
            response.status(400);
            response.type(APPLICATION_JSON);
            response.body(gson.toJson(new ErrorResponse(exception.getMessage(), FileWasDeletedException.class.getName())));
        });

        exception(Exception.class, (exception, request, response) -> {
            Gson gson = new Gson();
            response.status(500);
            response.type(APPLICATION_JSON);
            response.body(gson.toJson(new ErrorResponse(exception.getMessage(), exception.getClass().getName())));
        });


    }

    private void validateQuerryParams(Request req) {
        validateString(req.queryParams(DEP_SID), DEP_SID);
        validateString(req.queryParams(ARR_SID), ARR_SID);
    }

    private static boolean validateString(String paramValue, String paramName) throws WrongParamTypeException {
        if (StringUtils.isEmpty(paramValue))
            throw new WrongParamTypeException(String.format(S_PARAM_IS_NOT_INTEGEER_TYPE, paramName));
        if (!StringUtils.isNumeric(paramValue))
            throw new WrongParamTypeException(String.format(S_PARAM_IS_NOT_INTEGEER_TYPE, paramName));
        try {
            int i = Integer.parseInt(paramValue);
        } catch (Exception ex) {
            throw new WrongParamTypeException(String.format(S_PARAM_IS_NOT_INTEGEER_TYPE, paramName));
        }
        return true;
    }
}
