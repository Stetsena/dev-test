package restservice;

import ScalaBusRoot.BusRoutesService;
import restservice.controller.RoutesController;


public class RestService {


    public static void main(String[] args) throws Exception{
        if(args.length==0) throw new RuntimeException("Please provide first argument as path to routes file");
        BusRoutesService.init(null, args[0]);
        new RoutesController();
    }



}