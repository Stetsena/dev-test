package restservice.responce;

/**
 * Created by andrew on 11/2/16.
 */
public class ErrorResponse {

    public ErrorResponse() {
    }

    public ErrorResponse(String errorMessage, String exceptionType) {
        this.errorMessage = errorMessage;
        this.exceptionType = exceptionType;
    }

    String errorMessage;
    String exceptionType;

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }


    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
