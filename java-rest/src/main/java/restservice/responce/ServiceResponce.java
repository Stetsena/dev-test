package restservice.responce;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by andrew on 11/2/16.
 */
public class ServiceResponce {

    public ServiceResponce() {
    }

    public ServiceResponce(String dep_sid, String arr_sid, boolean direct_bus_route) {
        this.dep_sid = Integer.parseInt(dep_sid);
        this.arr_sid = Integer.parseInt(arr_sid);
        this.direct_bus_route = direct_bus_route;
    }

    int dep_sid;

    int arr_sid;

    boolean direct_bus_route;

    public int getDep_sid() {
        return dep_sid;
    }

    public void setDep_sid(int dep_sid) {
        this.dep_sid = dep_sid;
    }

    public int getArr_sid() {
        return arr_sid;
    }

    public void setArr_sid(int arr_sid) {
        this.arr_sid = arr_sid;
    }

    public boolean isDirect_bus_route() {
        return direct_bus_route;
    }

    public void setDirect_bus_route(boolean direct_bus_route) {
        this.direct_bus_route = direct_bus_route;
    }
}
