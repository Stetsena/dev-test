package restservice.exception;

/**
 * Created by andrew on 11/2/16.
 */
public class WrongParamTypeException extends RuntimeException {
    String message;

    public WrongParamTypeException(String message) {
        super(message);
        this.message = message;
    }

}
