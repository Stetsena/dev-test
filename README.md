# Bus Route Challenge

Implemented and ready for use

### Prerequisites

Please make sure you have Docker installed on your environment!

And since project using Spark api please set docker RAM at least 3-4 GB!

That's all requirement if you are going to run in docker.

To be able to run jar file locally please first install latest version of Spark 
and run it in local mode:

```
$YOUR_SPARK_DIR/sbin/start-master.sh
```

### Installing

You have your docker with 3-4 GB RAM running? Ok, then clone this repo to your local:

```
git clone https://Stetsena@bitbucket.org/Stetsena/dev-test.git dev-test 
```

Then go to /bin folder

```
cd ./dev-test/bin
```

Execute build docker script and provide your data file as argument (use chmod if need to set permissions)

```
./buld_docker_image.sh {path/to/your/data/file}
```

When it's finished just run container with next script

```
./run_container.sh start
```

Whenever you feel to stop it

```
./run_container.sh stop
```

