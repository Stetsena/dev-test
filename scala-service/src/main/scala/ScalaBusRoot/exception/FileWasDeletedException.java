package ScalaBusRoot.exception;

/**
 * Created by andrew on 11/2/16.
 */
public class FileWasDeletedException extends RuntimeException{
    public FileWasDeletedException(String message) {
        super(message);
    }
}
