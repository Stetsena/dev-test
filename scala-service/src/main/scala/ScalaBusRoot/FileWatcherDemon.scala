package ScalaBusRoot

/**
 * Created by andrew on 11/1/16.
 */


import java.io.FileNotFoundException

import better.files.{File, ThreadBackedFileMonitor}

object FileWatcherDemon {


  def start(fileLocation : String) {
    val file = File(fileLocation)
    val watcher = new ThreadBackedFileMonitor(file, recursive = true) {
      override def onCreate(file: File) = {
        BusRoutesService.cacheRoutesRdd(fileLocation)
        BusRoutesService.setFileExists(true);
      }

      override def onModify(file: File) = {
        BusRoutesService.cacheRoutesRdd(fileLocation)
      }

      override def onDelete(file: File) = {
           BusRoutesService.setFileExists(false);
      }
    }
    watcher.start()
  }

}
