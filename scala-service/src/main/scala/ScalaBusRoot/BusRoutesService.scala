package ScalaBusRoot

import java.io.FileNotFoundException

import ScalaBusRoot.exception.FileWasDeletedException
import better.files.File
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkContext, SparkConf}


object BusRoutesService {

  private val SPACE: String = " "
  private val LOCAL: String = "local"
  private val SPARK_APP_NAME: String = "SparkBusRouteSearcher"
  private val FILE_NOT_EXISTS_ERROR: String = "PLease provide file path!"
  private val FILE_WAS_DELETED: String = "File Was Deleted, please create or copy it to provided folder"
  private val FILE_DOES_NOT_EXISTS: String = "File Does Not Exists or Empty!"
  private var sc : SparkContext = null
  private var cachedRoutes: RDD[(String, Array[String])] = null
  private var isDirectRouteExist :(String, String) => Boolean = null

  def init(sparkIp: String, fileLocation: String){
    validateFile(fileLocation)
    launchSpark(Option(sparkIp))
    initiateRDD(Option(fileLocation))
  }

  def isDirectRouteExists(firstStation: String, secondStation: String): Boolean = {
    this.isDirectRouteExist(firstStation, secondStation)
  }

  def cacheRoutesRdd(fileLocation: String){
    cachedRoutes = sc.textFile(fileLocation)
      .mapPartitionsWithIndex(
        (i, iterator) =>
          if (i == 0 && iterator.hasNext) {
            iterator.next
            iterator
          } else iterator)
      .map(_.split(SPACE))
      .map(p => (p(0), p.drop(1)))
      .cache();
  }

  def setFileExists(exist: Boolean){
    exist match {
      case true => { this.isDirectRouteExist = (firstStation: String, secondStation: String) => {
        cachedRoutes.filter(element => element._2.contains(firstStation) && element._2.contains(secondStation)).count()>0
      }}
      case false => {
        this.isDirectRouteExist = (firstStation: String, secondStation: String) => {
          throw new FileWasDeletedException(FILE_WAS_DELETED)
        }
      }
    }
  }

  private def validateFile(fileLocation: String) {
    val file = File(fileLocation)
    if (file.isEmpty) throw new FileNotFoundException(FILE_DOES_NOT_EXISTS)
    setFileExists(true)
  }


  private def initiateRDD(fileLocation: Option[String]): Unit = {
    fileLocation match {
      case None => throw new FileNotFoundException(FILE_NOT_EXISTS_ERROR)
      case Some(s) => {
        FileWatcherDemon.start(s)
        cacheRoutesRdd(s)
      }
    }
  }

  private def launchSpark(sparkIp: Option[String]){
    sparkIp match {
      case None => this.sc = new SparkContext(new SparkConf().setAppName(SPARK_APP_NAME).setMaster(LOCAL))
      case Some(s) => this.sc = new SparkContext(new SparkConf().setAppName(SPARK_APP_NAME).setMaster(s))
    }
  }

}
