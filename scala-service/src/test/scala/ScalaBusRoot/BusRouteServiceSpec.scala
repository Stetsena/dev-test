package ScalaBusRoot


import java.io.FileNotFoundException

import ScalaBusRoot.exception.FileWasDeletedException
import better.files.File
import org.scalatest._
import concurrent.Eventually._
import org.scalatest.time.{Minutes, Span}


class BusRouteServiceSpec extends FlatSpec with Matchers{

  "First we need to create file that" should "be accesible for spark" in {
    val file = File("test-routes.csv").createIfNotExists()
    file.appendLines("3", "0 0 1 2 3 4", "1 3 1 6 5 4 9", "2 0 6 4")
  }

  "BusRouteService" should "not init and throw exception in case of non existing file provided" in {
    a [FileNotFoundException] should be thrownBy {
      BusRoutesService.init(null, "fake.csv")
    }
  }

  it should "init and throw no exceptions" in {
    val pathFile = File("test-routes.csv").path.toAbsolutePath.toString
    BusRoutesService.init(null, pathFile)
  }

  it should "respond with true if there is direct route exists in a file" in {
     BusRoutesService.isDirectRouteExists("3","0") should be (true)
  }

  it should "respond with false if there is no direct route exists in a file" in {
    BusRoutesService.isDirectRouteExists("3","94") should be (false)
  }

  it should "eventually throw an FileWasDeletedException if file was removed" in {
    File("test-routes.csv").delete()
    eventually (timeout(Span(4, Minutes))){
      a[FileWasDeletedException] should be thrownBy {
        BusRoutesService.isDirectRouteExists("3", "0")
      }
    }
  }

  it should "eventually return the result when data file was re-created" in {
    val file = File("test-routes.csv").createIfNotExists()
    file.appendLines("3", "0 0 1 2 3 4", "1 3 1 6 5 4 9", "2 0 6 4")
    eventually (timeout(Span(4, Minutes))){
      BusRoutesService.isDirectRouteExists("3","0") should be (true)
    }
  }

  it should "eventually return the result from recently updated file" in {
    File("test-routes.csv").createIfNotExists().appendLine("3 120 765 49")
    eventually (timeout(Span(4, Minutes))){
      BusRoutesService.isDirectRouteExists("120","765") should be (true)
    }
  }

  "In the end test data file " should "be removed" in {
    File("test-routes.csv").delete()
  }

}
