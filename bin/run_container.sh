#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PIDFILE=/tmp/$NAME.pid
LOGFILE=/tmp/$NAME.log

start() {
    if [ -f $PIDFILE ]; then
        if [ $(docker inspect --format="{{ .State.Running }}" $(cat $PIDFILE)) == "true" ]; then
            echo 'Service already running' >&2
            return 1
        else
            rm -f $PIDFILE
        fi
    fi
    docker run -d -p 8088:8088 dev-test &> $LOGFILE
    docker ps -a -q --filter ancestor=dev-test --format="{{.ID}}" &> $PIDFILE
    echo 'Service successfully started' >&2
}

stop() {
    if [ ! -f $PIDFILE ] || [ $(docker inspect --format="{{ .State.Running }}" $(cat $PIDFILE)) == "false" ]; then
        echo 'Service not running' >&2
        return 1
    fi
    docker stop $(cat $PIDFILE) && docker rm $(cat $PIDFILE) && rm -f $PIDFILE
    echo 'Service successfully stopped' >&2
}


case $1 in
    start)
        start
        ;;
    stop)
        stop
        ;;
    *)
        echo "Usage: $0 {start|stop}"
esac