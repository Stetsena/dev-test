#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd $DIR > /dev/null
DOCKER_FILE_DIR=../docker
if [ "$#" -ne 1 ] && [ -f $1 ]; then
    echo "# Please provide next param: "
    echo "# - Path to your file with data (WARN: Have to be a simple text file!)"
    echo "# Usage: $0 DATA_FILE"
else
cp $1 $DOCKER_FILE_DIR/routes.csv
docker build -t dev-test --build-arg repo=https://Stetsena@bitbucket.org/Stetsena/dev-test.git --build-arg file=routes.csv  $DOCKER_FILE_DIR
fi
popd > /dev/null